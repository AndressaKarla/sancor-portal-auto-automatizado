cotacao_conta_cliente_page = CotacaoContaClientePage.new

Dado("que eu tenha feito o contexto com as pré-condições") do

end

Quando("eu clicar na opção +COTAÇÃO") do
  cotacao_conta_cliente_page.wait_until_opcao_cotacao_visible
  cotacao_conta_cliente_page.opcao_cotacao.click
end

Então("deve ser apresentado a tela Nova cotação: pesquise a conta do cliente") do
  sleep 3
  expect(page.current_url).to have_content("/new-quote-account-search/")
  expect(cotacao_conta_cliente_page.titulo_nova_cotacao_pesquisa_conta_cliente.text).to have_content("Nova cotação: pesquise a conta do cliente")
end

