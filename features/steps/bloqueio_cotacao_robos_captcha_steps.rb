login_page = LoginPage.new
cotacao_fluxo_completo_page = CotacaoFluxoCompletoPage.new
bloqueio_cotacao_robos_capctha_page = BloqueioCotacaoRobosCapcthaPage.new

Dado("que eu esteja logado no GuideWire Policy Center") do

end

Dado("eu clique no menu Administração > Utilitários > Parâmetros de script") do

end

Dado("eu esteja na tela Parâmetros de script") do

end

Dado("eu esteja na Página Sete") do

end

Dado("o valor do parâmetro de script RecaptchaEnabled_GSS seja true") do

end

Dado("o valor do parâmetro de script RecaptchaSecret_GSS esteja configurado com uma chave secreta específica de outro ambiente diferente do ambiente que está sendo validado") do

end

Dado("eu clique no menu Administração > Usuários e Segurança > Organizações") do

end

Dado("eu pesquise a Organização que tenha vínculo com o mesmo usuário que eu vou logar no Portal Auto") do

end

Dado("eu esteja na seção Permissões") do

end

Dado("a opção Cotação via robô seja Não") do

end

Quando("eu informar no campo usuario o mesmo usuário vinculado a Organização configurada anteriormente {string}") do |usuario|
  login_page.campo_usuario.set usuario 
end

Quando("eu clicar no botão Avançar da etapa Veículo") do
  cotacao_fluxo_completo_page.botao_avancar_etapa_veiculo.click
end

Então("deve ser apresentado o modal Cotação") do
  sleep 5
  expect(bloqueio_cotacao_robos_capctha_page.modal_cotacao.text).to have_content("Cotação")
end

Então("deve ser apresentado a mensagem Não foi possível avançar com sua cotação por motivos técnicos - CZeroQuatro") do 
  expect(bloqueio_cotacao_robos_capctha_page.msg_motivos_tecnicos_c04.text).to have_content("Não foi possível avançar com sua cotação por motivos técnicos (C04)")
end