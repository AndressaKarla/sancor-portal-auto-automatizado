cotacao_fluxo_completo_page = CotacaoFluxoCompletoPage.new

Dado("eu clique na opção +COTAÇÃO") do
  cotacao_fluxo_completo_page.wait_until_opcao_cotacao_visible
  cotacao_fluxo_completo_page.opcao_cotacao.click
end

Dado("seja apresentado a tela Nova cotação: pesquise a conta do cliente") do
  sleep 3
  expect(page.current_url).to have_content("/new-quote-account-search/")
  expect(cotacao_fluxo_completo_page.titulo_tela_pesquisa_conta_cliente.text).to have_content("Nova cotação: pesquise a conta do cliente")
end

Dado("eu informe o campo CPF") do 
  cpf = Faker::CPF.numeric
  cotacao_fluxo_completo_page.campo_cpf.set cpf
end

Dado("eu informe o campo Nome {string}") do |nome|
  cotacao_fluxo_completo_page.campo_nome.set nome
end

Dado("eu informe o campo Sobrenome {string}") do |sobrenome|
  cotacao_fluxo_completo_page.campo_sobrenome.set sobrenome
end

Dado("eu clique no botão Pesquisar da tela Nova cotação") do
  cotacao_fluxo_completo_page.botao_pesquisar.click
end

Dado("seja apresentado a tela Resultados da pesquisa com a mensagem Nenhuma conta encontrada com base em seus critérios") do
  sleep 3
  expect(cotacao_fluxo_completo_page.titulo_tela_resultados_pesquisa.text).to have_content("Resultados da pesquisa")
  expect(cotacao_fluxo_completo_page.msg_nenhuma_conta_encontrada.text).to have_content("Nenhuma conta encontrada com base em seus critérios")
end

Dado("eu clique no botão Cadastrar novo da tela Resultados da pesquisa") do
  cotacao_fluxo_completo_page.botao_cadastrar_novo.click
end

Dado("seja apresentado a tela Nova cotação: detalhes da nova conta") do
  sleep 3
  expect(cotacao_fluxo_completo_page.titulo_tela_detalhes_nova_conta.text).to have_content("Nova cotação: detalhes da nova conta")
end

Dado("eu informe o campo Data de nascimento {string}") do |dataNascimento|
  cotacao_fluxo_completo_page.campo_data_nascimento.set dataNascimento
  cotacao_fluxo_completo_page.titulo_tela_detalhes_nova_conta.click
end

Dado("eu informe o campo Sexo {string}") do |sexo|
  cotacao_fluxo_completo_page.campo_sexo.select sexo
end

Dado("eu informe o campo Estado civil {string}") do |estadoCivil|
  cotacao_fluxo_completo_page.campo_estado_civil.select estadoCivil
end

Dado("eu informe o campo CEP {string}") do |cep|
  cotacao_fluxo_completo_page.campo_cep.set cep
end

Dado("eu informe o campo Número {string}") do |numero|
  cotacao_fluxo_completo_page.campo_numero.set numero
end

Dado("eu informe o campo Tipo de endereço {string}") do |tipoEndereco|
  cotacao_fluxo_completo_page.campo_tipo_endereco.select tipoEndereco
end

Dado("eu informe o campo Vendedor {string}") do |vendedor|
  cotacao_fluxo_completo_page.campo_vendedor.select vendedor
end

Dado("eu clique no botão Avançar da tela Nova cotação: detalhes da nova conta") do
  cotacao_fluxo_completo_page.botao_avancar_tela_detalhes_nova_conta.click
end

Dado("seja apresentado a tela Nova cotação: Tipo de seguro") do
  sleep 3
  expect(cotacao_fluxo_completo_page.titulo_tela_tipo_seguro.text).to have_content("Nova cotação: Tipo de seguro")
end

Dado("o campo Produto esteja preenchido automaticamente com a opção {string}") do |produto|
  expect(cotacao_fluxo_completo_page.campo_produto.text).to have_content(produto)
end

Dado("eu clique no botão Avançar da tela Nova cotação: Tipo de seguro") do
  cotacao_fluxo_completo_page.botao_avancar_tela_tipo_seguro.click
end

Dado("seja apresentado a etapa Condutor") do
  sleep 3
  expect(cotacao_fluxo_completo_page.titulo_etapa_condutor.text).to have_content("Condutor")
end

Dado("eu clique no botão Avançar da etapa Condutor") do
  cotacao_fluxo_completo_page.botao_avancar_etapa_condutor.click
end

Dado("seja apresentado a etapa Veículo") do
  sleep 3
  expect(cotacao_fluxo_completo_page.titulo_etapa_veiculo.text).to have_content("Veículo")
end

Dado("eu informe o campo Fabricante {string}") do |fabricante|
  cotacao_fluxo_completo_page.campo_fabricante.select fabricante
end

Dado("eu informe o campo Modelo {string}") do |modelo|
  cotacao_fluxo_completo_page.campo_modelo.select modelo
end

Dado("eu informe o campo Ano fabricação {string}") do |anoFabricacao|
  cotacao_fluxo_completo_page.campo_ano_fabricacao.select anoFabricacao
end

Dado("eu informe o campo Dispositivo anti-furto {string}") do |dispositivoAntiFurto|
  cotacao_fluxo_completo_page.campo_dispositivo_antifurto.select dispositivoAntiFurto
end

# Dado("eu informe o campo Placa {string}") do |placa|
Dado("eu informe o campo Placa") do 
  def geraPlaca
    # gera 3 letras maiúsculas aleatórias
    letter1 = 65 + (rand() * (90 - 65)).to_i
    letter2 = 65 + (rand() * (90 - 65)).to_i
    letter3 = 65 + (rand() * (90 - 65)).to_i   

    # gera 4 números aleatórios
    number1 = (rand() * 10).to_i
    number2 = (rand() * 10).to_i
    number3 = (rand() * 10).to_i
    number4 = (rand() * 10).to_i

    return letter1.chr + letter2.chr + letter3.chr + number1.to_s + number2.to_s + number3.to_s + number4.to_s; 
  end

  placa = geraPlaca

  cotacao_fluxo_completo_page.campo_placa.set placa
end

Dado("eu informe o campo Chassi") do 
  require 'securerandom'
  # gera 17 números aleatórios
  chassi = (SecureRandom.random_number * (10**17)).round

  cotacao_fluxo_completo_page.campo_chassi.set chassi
end

Dado("eu informe no campo Reside com o principal condutor... a opção {string}") do |opcao|
  if opcao == "Não"
    page.execute_script("$('#binaryinputctrlx4Right').prop('aria-checked', true).trigger('click')")
    page.execute_script("$('#binaryinputctrlx4Right').prop('aria-checked', true).trigger('click')")
  end

  if opcao == "Sim"
    page.execute_script("$('#binaryinputctrlx4Left').prop('aria-checked', true).trigger('click')")
    page.execute_script("$('#binaryinputctrlx4Left').prop('aria-checked', true).trigger('click')")
  end
end

Dado("eu informe no campo Deseja estender cobertura securitária... a opção {string}") do |opcao|
  if opcao == "Não"
    page.execute_script("$('#binaryinputctrlx5Right').prop('aria-checked', true).trigger('click')")
    page.execute_script("$('#binaryinputctrlx5Right').prop('aria-checked', true).trigger('click')")
  end

  if opcao == "Sim"
    page.execute_script("$('#binaryinputctrlx5Left').prop('aria-checked', true).trigger('click')")
    page.execute_script("$('#binaryinputctrlx5Left').prop('aria-checked', true).trigger('click')")
  end
end

Dado("eu informe o campo Tipo de seguro {string}") do |tipoSeguro|
  cotacao_fluxo_completo_page.campo_tipo_seguro.select tipoSeguro
end

Dado("eu clique no botão Avançar da etapa Veículo") do
  cotacao_fluxo_completo_page.botao_avancar_etapa_veiculo.click
end

Dado("seja apresentado a etapa Cotação") do
  sleep 20
  expect(cotacao_fluxo_completo_page.titulo_etapa_cotacao.text).to have_content("Cotação")
end

Dado("eu clique no botão Preencher Proposta da etapa Cotação") do
  cotacao_fluxo_completo_page.botao_preencher_proposta_etapa_cotacao.click
end

Dado("seja apresentado a etapa Detalhes do Preenchimento de Proposta") do
  sleep 10
  expect(cotacao_fluxo_completo_page.titulo_etapa_detalhes_preenchimento_proposta.text).to have_content("Detalhes")
end

Dado("eu informe o campo E-mail principal do segurado {string} do Preenchimento de Proposta") do |emailPrincipalSegurado|
  cotacao_fluxo_completo_page.campo_email_principal_segurado_preenchimento_proposta.set emailPrincipalSegurado
end

Dado("eu informe o campo Telefone principal {string} do Preenchimento de Proposta") do |telefonePrincipal|
  cotacao_fluxo_completo_page.campo_telefone_principal_preenchimento_proposta.select telefonePrincipal
end

Dado("eu informe o campo Telefone residencial {string} do Preenchimento de Proposta") do |telefoneResidencial|
  cotacao_fluxo_completo_page.campo_telefone_residencial_preenchimento_proposta.set telefoneResidencial
end

Dado("eu informe o campo Celular {string} do Preenchimento de Proposta") do |celular|
  cotacao_fluxo_completo_page.campo_celular_preenchimento_proposta.set celular
end

Dado("eu informe o campo Número {string} do Preenchimento de Proposta") do |numero|
  cotacao_fluxo_completo_page.campo_numero_preenchimento_proposta.set numero
end

Dado("eu clique no botão Avançar da etapa Detalhes do Preenchimento de Proposta") do
  cotacao_fluxo_completo_page.botao_avancar_etapa_detalhes_preenchimento_proposta.click
end

Dado("seja apresentado a etapa Condutor do Preenchimento de Proposta") do
  sleep 7
  expect(cotacao_fluxo_completo_page.titulo_etapa_condutor_preenchimento_proposta.text).to have_content("Condutor")
end

Dado("eu clique no botão Avançar da etapa Condutor do Preenchimento de Proposta") do
  cotacao_fluxo_completo_page.botao_avancar_etapa_condutor_preenchimento_proposta.click
end

Dado("seja apresentado a etapa Veículo do Preenchimento de Proposta") do
  sleep 3
  expect(cotacao_fluxo_completo_page.titulo_etapa_veiculo_preenchimento_proposta.text).to have_content("Veículo")
end

Dado("eu informe o campo Vistoriador {string} do Preenchimento de Proposta") do |vistoriador|
  cotacao_fluxo_completo_page.campo_vistoriador_preenchimento_proposta.select vistoriador
end

Dado("eu clique no botão Avançar da etapa Veículo do Preenchimento de Proposta") do
  cotacao_fluxo_completo_page.botao_avancar_etapa_veiculo_preenchimento_proposta.click
end

Dado("seja apresentado a etapa Dados de pagamento do Preenchimento de Proposta") do
  sleep 3
  expect(cotacao_fluxo_completo_page.titulo_etapa_dados_pagamentos_preenchimento_proposta.text).to have_content("Dados de pagamento")
end

Dado("eu informe o campo Forma de pagamento {string} do Preenchimento de Proposta") do |formaPagamento|
  sleep 7
  cotacao_fluxo_completo_page.campo_forma_pagamento_preenchimento_proposta.select formaPagamento
end

Dado("eu informe o campo Banco {string} do Preenchimento de Proposta") do |banco|
  sleep 5
  cotacao_fluxo_completo_page.campo_banco_preenchimento_proposta.select banco
end

Dado("eu informe a opção {string} do Plano de pagamento do Preenchimento de Proposta") do |planoPagamento|
  page.execute_script(%{function _x() {
    var xresult = document.evaluate("/html/.//tr[1]/td[1]/input", document, null, XPathResult.ANY_TYPE, null);
    var xnodes = [];
    var xres;
    
    while (xres = xresult.iterateNext()) {
        xnodes.push(xres);
    }

    jQuery(function($) {
	   $(xnodes).prop('aria-checked', true).trigger('click');
	   $(xnodes).prop('aria-checked', true).trigger('click');
    });
}

$(_x());})
end

Quando("eu clicar no botão Gerar Proposta da etapa Dados de pagamento do Preenchimento de Proposta") do
  sleep 3
  cotacao_fluxo_completo_page.botao_gerar_proposta_etapa_dados_pagamentos_preenchimento_proposta.click
end

Então("deve ser apresentado a tela Proposta efetivada com sucesso!") do
  sleep 5
  expect(cotacao_fluxo_completo_page.titulo_tela_proposta_efetivada_sucesso.text).to have_content("Proposta efetivada com sucesso!")
end
