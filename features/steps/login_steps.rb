login_page = LoginPage.new

Dado("que eu esteja na tela de Login do Portal Auto") do
  login_page.load
end

Quando("eu informar no campo usuario {string}") do |usuario|
  login_page.campo_usuario.set usuario
end

Quando("eu informar no campo senha {string}") do |senha|
  login_page.campo_senha.set senha
end

Quando("eu clicar no botão Entrar") do
  login_page.botao_entrar.click
  # if(expect(login_page).to have_save_button.msg_erro_tentar_entrar)
  #   login_page.botao_entrar.click
  # end
end

Então("deve ser apresentado a Página Inicial") do
  sleep 10
  expect(page.current_url).to have_content("/home") 
  expect(login_page.tela_painel.text).to have_content("Painel")
end

Então("para o cenário {int} deve ser apresentado a mensagem {string}") do |cenario, mensagem|
  sleep 3
  if cenario == 1
    expect(login_page.msg_obrig_campo_usuario.text).to have_content(mensagem)
    expect(login_page.msg_obrig_campo_senha.text).to have_content(mensagem)
  end

  if cenario == 2
    expect(login_page.msg_obrig_campo_usuario.text).to have_content(mensagem)
  end

  if cenario == 3
    expect(login_page.msg_obrig_campo_senha.text).to have_content(mensagem)
  end

  if cenario == 4
    expect(login_page.msg_usuario_senha_invalidos.text).to have_content(mensagem)
  end
end

