require 'site_prism'

class CotacaoContaClientePage < SitePrism::Page
    # set_url "/#/"
    
    element :opcao_cotacao, '#page-inner > div.gw-snap-content > div > header > div > div.gw-header-nav-wrapper > div.gw-header-nav-right-part > ul > li > div.gw-btn-flat.gw-new-quote.float-right.ng-scope > a'
    element :titulo_nova_cotacao_pesquisa_conta_cliente, '#existing-account-search'
end 