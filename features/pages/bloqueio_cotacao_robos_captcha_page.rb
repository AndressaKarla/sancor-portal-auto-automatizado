require 'site_prism'

class BloqueioCotacaoRobosCapcthaPage < SitePrism::Page
    # Etapa "Veículo"
    element :modal_cotacao, 'body > div.gw-modal.gw-fade.gw-modal_animation_.in > div > div.gw-modal-header.ng-scope > h2'
    element :msg_motivos_tecnicos_c04, 'body > div.gw-modal.gw-fade.gw-modal_animation_.in > div > div.gw-modal-body.ng-scope > div > p:nth-child(1)'
end 