require 'site_prism'

class LoginPage < SitePrism::Page
    set_url "/#/"
    
    element :campo_usuario, 'input[name="username"]'
    element :campo_senha, 'input[name="password"]'
    element :botao_entrar, '.auth0-label-submit'
    element :msg_erro_tentar_entrar, '#widget > div > div > form > div > div > div > div > div.auth0-lock-content-body-wrapper > div:nth-child(1) > div > div > span > span'
    element :msg_obrig_campo_usuario, '#auth0-lock-error-msg-username'
    element :msg_obrig_campo_senha, '#auth0-lock-error-msg-password'
    element :msg_usuario_senha_invalidos, '#widget > div > div > form > div > div > div > div > div.auth0-lock-content-body-wrapper > div:nth-child(1) > div > div > span > span'
    element :tela_painel, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > section > div > div:nth-child(1) > gw-row > div:nth-child(1) > h1 > ng-transclude > span'
end