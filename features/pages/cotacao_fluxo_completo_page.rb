require 'site_prism'

class CotacaoFluxoCompletoPage < SitePrism::Page
    element :opcao_cotacao, '#page-inner > div.gw-snap-content > div > header > div > div.gw-header-nav-wrapper > div.gw-header-nav-right-part > ul > li > div.gw-btn-flat.gw-new-quote.float-right.ng-scope > a'
    
    # Tela "Nova cotação: pesquise a conta do cliente"
    element :titulo_tela_pesquisa_conta_cliente, '#existing-account-search'
    element :campo_cpf, '#cpf'
    element :campo_nome, '#firstName'
    element :campo_sobrenome, '#lastName'
    element :botao_pesquisar, '#form-existing-account-search > div > div.gw-page-buttons > button.gw-next.gw-btn-primary.gw-btn.ng-binding'

    # Tela "Resultados da pesquisa"
    element :titulo_tela_resultados_pesquisa, '#possible-account-matches > h1'
    element :msg_nenhuma_conta_encontrada, '#possible-account-matches > div > h4'
    element :botao_cadastrar_novo, '#possible-account-matches > section > div.gw-page-buttons > button.gw-next.gw-btn-primary.gw-btn.ng-binding'

    # Tela "Nova cotação: detalhes da nova conta"
    element :titulo_tela_detalhes_nova_conta, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > section > div > h1:nth-child(1)'
    element :campo_data_nascimento, '#localDateChooser'
    element :campo_sexo, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > section > div > section > ng-form > div:nth-child(2) > div:nth-child(3) > div > div > ng-transclude > select'
    element :campo_estado_civil, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > section > div > section > ng-form > div:nth-child(2) > div:nth-child(4) > div > div > ng-transclude > select'
    element :campo_cep, '#\30 '
    element :campo_numero, '#\34 '
    element :campo_tipo_endereco, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > section > div > section > ng-form > div:nth-child(4) > div > div:nth-child(3) > div > div > ng-transclude > select'
    element :campo_vendedor, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > section > div > section > ng-form > div:nth-child(5) > div > div > div > ng-transclude > select'
    element :botao_avancar_tela_detalhes_nova_conta, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > section > div > section > ng-form > div.gw-page-buttons > button.gw-next.gw-btn-primary.gw-btn.ng-binding'
    
    # Tela "Nova cotação: Tipo de seguro"
    element :titulo_tela_tipo_seguro, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > section > div > h1:nth-child(2)'
    element :campo_produto, '#ProductCode'
    element :botao_avancar_tela_tipo_seguro, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > section > div > section.gw-content.ng-scope.ng-isolate-scope.gw-loader-wrapper_block.gw-loader-wrapper_has-text > ng-form > div.gw-page-buttons > button.gw-next.gw-btn-primary.gw-btn.ng-binding'

    # Etapa "Condutor"
    element :titulo_etapa_condutor, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > div > div > div.gw-wizard-main > div.gw-page.gw-box.ng-isolate-scope.gw-loader-wrapper_block > div:nth-child(2) > div:nth-child(2) > div > h2'
    element :botao_avancar_etapa_condutor, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > div > div > div.gw-wizard-main > div.gw-page.gw-box.ng-isolate-scope.gw-loader-wrapper_block > div:nth-child(2) > div:nth-child(2) > div > ng-form > div.gw-page-buttons.ng-scope > button.gw-btn-primary.ng-binding.ng-isolate-scope.gw-loader-wrapper_inline'

    # Etapa "Veículo"
    element :titulo_etapa_veiculo, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > div > div > div.gw-wizard-main > div.gw-page.gw-box.ng-isolate-scope.gw-loader-wrapper_block.gw-loader-done > div:nth-child(2) > div:nth-child(2) > div > h2'
    element :campo_fabricante, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > div > div > div.gw-wizard-main > div.gw-page.gw-box.ng-isolate-scope.gw-loader-wrapper_block.gw-loader-done > div:nth-child(2) > div:nth-child(2) > div > ng-form > div.ng-isolate-scope > div > div:nth-child(3) > div > div > ng-transclude > div > div > select'
    element :campo_modelo, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > div > div > div.gw-wizard-main > div.gw-page.gw-box.ng-isolate-scope.gw-loader-wrapper_block.gw-loader-done > div:nth-child(2) > div:nth-child(2) > div > ng-form > div.ng-isolate-scope > div > div:nth-child(4) > div > div > ng-transclude > select'
    element :campo_ano_fabricacao, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > div > div > div.gw-wizard-main > div.gw-page.gw-box.ng-isolate-scope.gw-loader-wrapper_block.gw-loader-done > div:nth-child(2) > div:nth-child(2) > div > ng-form > div.ng-isolate-scope > div > div:nth-child(7) > div > div > ng-transclude > div > div > select'
    element :campo_dispositivo_antifurto, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > div > div > div.gw-wizard-main > div.gw-page.gw-box.ng-isolate-scope.gw-loader-wrapper_block.gw-loader-done > div:nth-child(2) > div:nth-child(2) > div > ng-form > div.ng-isolate-scope > div > div:nth-child(15) > div > div > ng-transclude > div > div > select'
    element :campo_placa, :xpath, './/*[@id="38"]'
    element :campo_chassi, :xpath, './/*[@id="39"]'
    element :campo_tipo_seguro, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > div > div > div.gw-wizard-main > div.gw-page.gw-box.ng-isolate-scope.gw-loader-wrapper_block.gw-loader-done > div:nth-child(2) > div:nth-child(2) > div > ng-form > div.ng-isolate-scope > div > div:nth-child(27) > div > div > ng-transclude > div > div > select'
    element :botao_avancar_etapa_veiculo, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > div > div > div.gw-wizard-main > div.gw-page.gw-box.ng-isolate-scope.gw-loader-wrapper_block.gw-loader-done > div:nth-child(2) > div:nth-child(2) > div > ng-form > div.gw-page-buttons.ng-scope > button.gw-btn-primary.ng-binding.ng-isolate-scope.gw-loader-wrapper_inline'

    # Etapa "Cotação"
    element :titulo_etapa_cotacao, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > div > div > div.gw-wizard-main > div.gw-page.gw-box.ng-isolate-scope.gw-loader-wrapper_block.gw-loader-done > div:nth-child(2) > div:nth-child(2) > div > div.gw-page-section-title.gw-page-section-title__area > div'
    element :botao_preencher_proposta_etapa_cotacao, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > div > div > div.gw-wizard-main > div.gw-page.gw-box.ng-isolate-scope.gw-loader-wrapper_block.gw-loader-done > div:nth-child(2) > div:nth-child(2) > div > div.gw-tabbable.ng-scope.ng-isolate-scope > div > div.gw-tab-pane.ng-scope.gw-active > div > form > div > div:nth-child(2) > div > div.gw-panel-item.gw-custom-quote > div.gw-quote-box.ng-isolate-scope.displayed > button'

    # Etapa "Detalhes" do Preenchimento de proposta
    element :titulo_etapa_detalhes_preenchimento_proposta, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > div > div > div.gw-wizard-main > div.gw-page.gw-box.ng-isolate-scope.gw-loader-wrapper_block.gw-loader-done > div:nth-child(2) > div:nth-child(2) > div > ng-form > h1'
    element :campo_email_principal_segurado_preenchimento_proposta, :xpath, './/*[@id="109"]'
    element :campo_telefone_principal_preenchimento_proposta, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > div > div > div.gw-wizard-main > div.gw-page.gw-box.ng-isolate-scope.gw-loader-wrapper_block.gw-loader-done > div:nth-child(2) > div:nth-child(2) > div > ng-form > div:nth-child(13) > div > div > ng-transclude > select'
    element :campo_telefone_residencial_preenchimento_proposta, :xpath, './/*[@id="125"]'
    element :campo_celular_preenchimento_proposta, :xpath, './/*[@id="126"]'
    element :campo_numero_preenchimento_proposta, :xpath, './/*[@id="117"]'
    element :botao_avancar_etapa_detalhes_preenchimento_proposta, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > div > div > div.gw-wizard-main > div.gw-page.gw-box.ng-isolate-scope.gw-loader-wrapper_block.gw-loader-done > div:nth-child(2) > div:nth-child(2) > div > ng-form > div.gw-page-buttons > button.gw-btn-primary.ng-binding.ng-isolate-scope.gw-loader-wrapper_inline'

    # Etapa "Condutor" do Preenchimento de proposta
    element :titulo_etapa_condutor_preenchimento_proposta, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > div > div > div.gw-wizard-main > div.gw-page.gw-box.ng-isolate-scope.gw-loader-wrapper_block.gw-loader-done > div:nth-child(2) > div:nth-child(2) > div > h2'
    element :botao_avancar_etapa_condutor_preenchimento_proposta, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > div > div > div.gw-wizard-main > div.gw-page.gw-box.ng-isolate-scope.gw-loader-wrapper_block.gw-loader-done > div:nth-child(2) > div:nth-child(2) > div > ng-form > div.gw-page-buttons > button.gw-btn-primary.ng-binding.ng-isolate-scope.gw-loader-wrapper_inline'

    # Etapa "Veículo" do Preenchimento de proposta
    element :titulo_etapa_veiculo_preenchimento_proposta, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > div > div > div.gw-wizard-main > div.gw-page.gw-box.ng-isolate-scope.gw-loader-wrapper_block.gw-loader-done > div:nth-child(2) > div:nth-child(2) > div > h2'
    element :campo_vistoriador_preenchimento_proposta, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > div > div > div.gw-wizard-main > div.gw-page.gw-box.ng-isolate-scope.gw-loader-wrapper_block.gw-loader-done > div:nth-child(2) > div:nth-child(2) > div > ng-form > div:nth-child(1) > gss-inspector > div > div.ng-isolate-scope > div > div > ng-transclude > select'
    element :botao_avancar_etapa_veiculo_preenchimento_proposta, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > div > div > div.gw-wizard-main > div.gw-page.gw-box.ng-isolate-scope.gw-loader-wrapper_block.gw-loader-done > div:nth-child(2) > div:nth-child(2) > div > ng-form > div.gw-page-buttons > button.gw-btn-primary.ng-binding.ng-isolate-scope.gw-loader-wrapper_inline'

    # Etapa "Dados de pagamento" do Preenchimento de proposta
    element :titulo_etapa_dados_pagamentos_preenchimento_proposta, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > div > div > div.gw-wizard-main > div.gw-page.gw-box.ng-isolate-scope.gw-loader-wrapper_block.gw-loader-done > div:nth-child(2) > div:nth-child(2) > div > form > pa-payment-edit-gss > h2'
    element :campo_forma_pagamento_preenchimento_proposta, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > div > div > div.gw-wizard-main > div.gw-page.gw-box.ng-isolate-scope.gw-loader-wrapper_block.gw-loader-done > div:nth-child(2) > div:nth-child(2) > div > form > pa-payment-edit-gss > div.gw-payment-options.ng-isolate-scope.gw-loader-wrapper_block > div:nth-child(1) > div > div > ng-transclude > select'
    element :campo_banco_preenchimento_proposta, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > div > div > div.gw-wizard-main > div.gw-page.gw-box.ng-isolate-scope.gw-loader-wrapper_block.gw-loader-done > div:nth-child(2) > div:nth-child(2) > div > form > pa-payment-edit-gss > div.gw-payment-options.ng-isolate-scope.gw-loader-wrapper_block > div.ng-scope.ng-isolate-scope > div > div > select'
    element :botao_gerar_proposta_etapa_dados_pagamentos_preenchimento_proposta, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > div > div > div.gw-wizard-main > div.gw-page.gw-box.ng-isolate-scope.gw-loader-wrapper_block.gw-loader-done > div:nth-child(2) > div:nth-child(2) > div > form > div > div > button.gw-btn-primary.ng-binding'

    # Tela "Proposta efetivada com sucesso!"
    element :titulo_tela_proposta_efetivada_sucesso, '#page-inner > div.gw-snap-content > div > main > div > div.gw-content-wrapper > div > div > h1'
end 