# language: pt

Funcionalidade: Cotação fluxo completo
    Sendo um usuario
    Quero clicar na opção +COTAÇÃO
    Para que eu possa realizar o fluxo completo de cotação

Contexto: Pré condições para realizar Cotação fluxo completo
    # login
    Dado que eu esteja na tela de Login do Portal Auto
    Quando eu informar no campo usuario "4340"
    E eu informar no campo senha "Sancor123"
    E eu clicar no botão Entrar
    Então deve ser apresentado a Página Inicial


@cotacaoFluxoCompleto
Cenario: Verificando tela Nova cotação - pesquise a conta do cliente
    Dado que eu tenha feito o contexto com as pré-condições
    E eu clique na opção +COTAÇÃO
    E seja apresentado a tela Nova cotação: pesquise a conta do cliente
    E eu informe o campo CPF 
    E eu informe o campo Nome "Teste"
    E eu informe o campo Sobrenome "Automatizado Portal"
    E eu clique no botão Pesquisar da tela Nova cotação
    E seja apresentado a tela Resultados da pesquisa com a mensagem Nenhuma conta encontrada com base em seus critérios
    E eu clique no botão Cadastrar novo da tela Resultados da pesquisa

    E seja apresentado a tela Nova cotação: detalhes da nova conta
    E eu informe o campo Data de nascimento "08/04/1995"
    E eu informe o campo Sexo "Feminino"
    E eu informe o campo Estado civil "Solteiro"
    E eu informe o campo CEP "87035230"
    E eu informe o campo Número "3258"
    E eu informe o campo Tipo de endereço "Residencial"
    E eu informe o campo Vendedor "4340 440"
    E eu clique no botão Avançar da tela Nova cotação: detalhes da nova conta

    E seja apresentado a tela Nova cotação: Tipo de seguro
    E o campo Produto esteja preenchido automaticamente com a opção "Automóvel"
    E eu clique no botão Avançar da tela Nova cotação: Tipo de seguro

    E seja apresentado a etapa Condutor
    E eu clique no botão Avançar da etapa Condutor

    E seja apresentado a etapa Veículo
    E eu informe o campo Fabricante "GM - Chevrolet"
    E eu informe o campo Modelo "ONIX Lollapalooza 1.0 F.Power 5p Mec."
    E eu informe o campo Ano fabricação "2014"
    E eu informe o campo Dispositivo anti-furto "Comum"
    E eu informe o campo Placa
    E eu informe o campo Chassi
    E eu informe no campo Reside com o principal condutor... a opção "Não"
    E eu informe no campo Deseja estender cobertura securitária... a opção "Não"
    E eu informe o campo Tipo de seguro "Seguro novo"
    E eu clique no botão Avançar da etapa Veículo

    E seja apresentado a etapa Cotação
    E eu clique no botão Preencher Proposta da etapa Cotação

    E seja apresentado a etapa Detalhes do Preenchimento de Proposta
    E eu informe o campo E-mail principal do segurado "testesegurado@yahoo.com" do Preenchimento de Proposta
    E eu informe o campo Telefone principal "Celular" do Preenchimento de Proposta
    E eu informe o campo Telefone residencial "4430364578" do Preenchimento de Proposta
    E eu informe o campo Celular "44991624578" do Preenchimento de Proposta
    E eu informe o campo Número "4568" do Preenchimento de Proposta
    E eu clique no botão Avançar da etapa Detalhes do Preenchimento de Proposta

    E seja apresentado a etapa Condutor do Preenchimento de Proposta
    E eu clique no botão Avançar da etapa Condutor do Preenchimento de Proposta

    E seja apresentado a etapa Veículo do Preenchimento de Proposta
    E eu informe o campo Vistoriador "Brochweld Vistorias" do Preenchimento de Proposta
    E eu clique no botão Avançar da etapa Veículo do Preenchimento de Proposta

    E seja apresentado a etapa Dados de pagamento do Preenchimento de Proposta
    E eu informe o campo Forma de pagamento "Boleto" do Preenchimento de Proposta
    E eu informe o campo Banco "756 - SICOOB" do Preenchimento de Proposta
    E eu informe a opção "B – à vista" do Plano de pagamento do Preenchimento de Proposta

    Quando eu clicar no botão Gerar Proposta da etapa Dados de pagamento do Preenchimento de Proposta
    Então deve ser apresentado a tela Proposta efetivada com sucesso!


    



