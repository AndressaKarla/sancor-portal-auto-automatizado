# language: pt

Funcionalidade: Bloqueio de cotação para robôs por meio do captcha
    Sendo um usuario
    Quero clicar na opção +COTAÇÃO
    Para que eu possa validar o bloqueio de cotação para robôs por meio do captcha

Contexto: Pré condições para realizar bloqueio de cotação para robôs por meio do captcha
    # Configurações de Parâmetros de script e da Organização no Policy Center para bloqueio de cotação 
    # Esses passos abaixo precisam ser executados manualmente
    Dado que eu esteja logado no GuideWire Policy Center 
    E eu clique no menu Administração > Utilitários > Parâmetros de script
    E eu esteja na tela Parâmetros de script
    E eu esteja na Página Sete
    E o valor do parâmetro de script RecaptchaEnabled_GSS seja true
    E o valor do parâmetro de script RecaptchaSecret_GSS esteja configurado com uma chave secreta específica de outro ambiente diferente do ambiente que está sendo validado 
        # INT: 6Le5M-0ZAAAAAIEHreXI421qVZumb26eT3e7pzbT configurado no TST ou
        # TST: 6Ld1Me0ZAAAAALQnJMgmhM-GrONYp3pJ4YY3hZnP configurado no INT
    E eu clique no menu Administração > Usuários e Segurança > Organizações
    E eu pesquise a Organização que tenha vínculo com o mesmo usuário que eu vou logar no Portal Auto
        # UNICOOB CORRETORA DE SEGUROS LTDA
        # Nome de usuário: 4340
    E eu esteja na seção Permissões
    E a opção Cotação via robô seja Não

    # login
    Dado que eu esteja na tela de Login do Portal Auto
    Quando eu informar no campo usuario o mesmo usuário vinculado a Organização configurada anteriormente "4340"
    E eu informar no campo senha "Sancor123"
    E eu clicar no botão Entrar
    Então deve ser apresentado a Página Inicial


@bloqueioCotacaoRobos
Cenario: Verificando mensagem e código de bloqueio de cotação para robôs 
    Dado que eu tenha feito o contexto com as pré-condições
    E eu clique na opção +COTAÇÃO
    E seja apresentado a tela Nova cotação: pesquise a conta do cliente
    E eu informe o campo CPF 
    E eu informe o campo Nome "Teste"
    E eu informe o campo Sobrenome "Automatizado Portal"
    E eu clique no botão Pesquisar da tela Nova cotação
    E seja apresentado a tela Resultados da pesquisa com a mensagem Nenhuma conta encontrada com base em seus critérios
    E eu clique no botão Cadastrar novo da tela Resultados da pesquisa

    E seja apresentado a tela Nova cotação: detalhes da nova conta
    E eu informe o campo Data de nascimento "08/04/1995"
    E eu informe o campo Sexo "Feminino"
    E eu informe o campo Estado civil "Solteiro"
    E eu informe o campo CEP "87035230"
    E eu informe o campo Número "3258"
    E eu informe o campo Tipo de endereço "Residencial"
    E eu informe o campo Vendedor "4340 440"
    E eu clique no botão Avançar da tela Nova cotação: detalhes da nova conta

    E seja apresentado a tela Nova cotação: Tipo de seguro
    E o campo Produto esteja preenchido automaticamente com a opção "Automóvel"
    E eu clique no botão Avançar da tela Nova cotação: Tipo de seguro

    E seja apresentado a etapa Condutor
    E eu clique no botão Avançar da etapa Condutor

    E seja apresentado a etapa Veículo
    E eu informe o campo Fabricante "GM - Chevrolet"
    E eu informe o campo Modelo "ONIX Lollapalooza 1.0 F.Power 5p Mec."
    E eu informe o campo Ano fabricação "2014"
    E eu informe o campo Dispositivo anti-furto "Comum"
    E eu informe o campo Placa
    E eu informe o campo Chassi
    E eu informe no campo Reside com o principal condutor... a opção "Não"
    E eu informe no campo Deseja estender cobertura securitária... a opção "Não"
    E eu informe o campo Tipo de seguro "Seguro novo"

    Quando eu clicar no botão Avançar da etapa Veículo

    Então deve ser apresentado o modal Cotação
    E deve ser apresentado a mensagem Não foi possível avançar com sua cotação por motivos técnicos - CZeroQuatro


    



