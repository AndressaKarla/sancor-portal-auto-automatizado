# language: pt

@login
Funcionalidade: Login do Portal Auto
    Sendo um usuario
    Quero fazer login no Portal Auto
    Para que eu possa utilizar o sistema

@loginValido
Cenario: Efetuando login com usuário e senha válidos do Portal Auto
    Dado que eu esteja na tela de Login do Portal Auto
    Quando eu informar no campo usuario "4340"
    E eu informar no campo senha "Sancor123"
    E eu clicar no botão Entrar
    Então deve ser apresentado a Página Inicial 

@loginInvalido
Esquema do Cenário: Efetuando login com usuário ou senha inválidos do Portal Auto
    Dado que eu esteja na tela de Login do Portal Auto
    Quando eu informar no campo usuario <usuario>
    E eu informar no campo senha <senha>
    E eu clicar no botão Entrar
    Então para o cenário <cenario> deve ser apresentado a mensagem <mensagem> 

    Exemplos:
        | cenario   |   usuario     |  senha        | mensagem                            | 
        |    1      |    ""         |    ""         |  "Não pode estar em branco"         | 
        |    2      |    ""         | "Sancor123"   |  "Não pode estar em branco"         | 
        |    3      |   "4340"      |     ""        |  "Não pode estar em branco"         | 
        |    4      |   "4340"      | "Sancor1"     |  "USUÁRIO OU SENHA INVÁLIDOS."      |     
