# language: pt

@cotacaoContaCliente
Funcionalidade: Nova cotação - pesquise a conta do cliente
    Sendo um usuario
    Quero clicar na opção +COTAÇÃO
    Para que eu possa verificar a tela Nova cotação - pesquise a conta do cliente

Contexto: Pré condições para acessar Nova cotação - pesquise a conta do cliente
    # login
    Dado que eu esteja na tela de Login do Portal Auto
    Quando eu informar no campo usuario "4340"
    E eu informar no campo senha "Sancor123"
    E eu clicar no botão Entrar
    Então deve ser apresentado a Página Inicial

@cotacaoContaCliente1
Cenario: Verificando tela Nova cotação - pesquise a conta do cliente
    Dado que eu tenha feito o contexto com as pré-condições
    Quando eu clicar na opção +COTAÇÃO
    Então deve ser apresentado a tela Nova cotação: pesquise a conta do cliente
