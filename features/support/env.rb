require 'capybara'
require 'capybara/cucumber'
require "selenium-webdriver"
require 'cpf_faker'

require_relative 'helper.rb'
require_relative 'spec_helper.rb'

BROWSER = ENV['BROWSER']
ENVIRONMENT_TYPE = ENV['ENVIRONMENT_TYPE']



Capybara.register_driver :chrome_headless do |app|
    capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
      chromeOptions: {
        args: %w[ no-sandbox headless disable-gpu window-size=1280,1024]
        # args: %w[ no-sandbox headless disable-gpu start-fullscreen]
      }
    )
  
    Capybara::Selenium::Driver.new(app, browser: :chrome, desired_capabilities: capabilities)
  end

  # Capybara.register_driver :chrome do |app|
  #   capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
  #     chromeOptions: {
  #       args: %w[ no-sandbox disable-gpu window-size=1280,1024]
  #       # args: %w[ no-sandbox disable-gpu start-maximized]
  #       # args: %w[ no-sandbox disable-gpu viewport-maximize]
  #       # args: %w[ start-fullscreen no-sandbox disable-gpu ]
  #     }
  #   )
  
  #   Capybara::Selenium::Driver.new(app, browser: :chrome, desired_capabilities: capabilities)
  # end

  # log-level: 
  # Sets the minimum log level.
  # Valid values are from 0 to 3: 
      # INFO = 0
      # WARNING = 1 
      # LOG_ERROR = 2 
      # LOG_FATAL = 3
  # default is 0.
  LOGLEVEL = 2

  Capybara.register_driver :chrome do |app|
    caps = Selenium::WebDriver::Remote::Capabilities.chrome(loggingPrefs: { browser: 'ALL' })
    opts = Selenium::WebDriver::Chrome::Options.new
  
    chrome_args = %w[--window-size=1366,768 --disable-gpu --no-sandbox --disable-dev-shm-usage]
    chrome_args.each { |arg| opts.add_argument(arg) }
  
    opts.add_argument('log-level=' + LOGLEVEL.to_s)
    
    Capybara::Selenium::Driver.new(app, browser: :chrome, options: opts, desired_capabilities: caps)
  end

  # Capybara.register_driver :chrome_headless do |app|
  #   caps = Selenium::WebDriver::Remote::Capabilities.chrome(loggingPrefs: { browser: 'ALL' })
  #   opts = Selenium::WebDriver::Chrome::Options.new
  
  #   chrome_args = %w[--headless --window-size=1920,1080 --disable-gpu --no-sandbox --disable-dev-shm-usage]
  #   chrome_args.each { |arg| opts.add_argument(arg) }
  
  #   opts.add_argument('log-level=' + LOGLEVEL.to_s)
    
  #   Capybara::Selenium::Driver.new(app, browser: :chrome, options: opts, desired_capabilities: caps)
  # end

  Capybara.register_driver :firefox do |app|
    options = {
        :js_errors => true,
            :timeout => 360,
                :debug => false,
                    :inspector => false,
            }
    Capybara::Selenium::Driver.new(app, :browser => :firefox)
    end

  

## Cucumber Definitions
Before do |feature|
    ## configure the chosen browser
    Capybara.configure do |capybara|
      #Define o browser utilizado no teste
        if BROWSER.eql?('chrome')
            capybara.default_driver = :chrome
        elsif BROWSER.eql?('chrome_headless')
            capybara.default_driver = :chrome_headless
        else BROWSER.eql?('chrome_headless')
            capybara.default_driver = :firefox
        end

        #define o ambiente a ser testado
        if ENVIRONMENT_TYPE.eql?('qa')
          ## Ambiente TST
           capybara.app_host = 'https://portalbrasil-tst.gruposancorseguros.com/gateway-portal/dist/html'

          ## Ambiente INT-DEV
          # capybara.app_host = 'https://portalbrasil-dev.gruposancorseguros.com/gateway-portal/html'

          ## Ambiente INT
         # capybara.app_host = 'https://portalbrasil-pre.gruposancorseguros.com/gateway-portal/dist/html'
        else ENVIRONMENT_TYPE.eql?('dev')
          capybara.app_host = 'http://dev.seuambiente.com.br'
        end
      end
  
    ## set default max wait
    Capybara.default_max_wait_time = 75
  end


  After do |scenario|
    ## take screenshot
    scenario_name = scenario.name.gsub(/\s+/, '_').tr('/', '_')
    if scenario.passed?
      take_screenshot(scenario_name, 'passed')
    else
      take_screenshot(scenario_name, 'failed')
    end
    ## kills instance when not headless
      Capybara.current_session.driver.quit
  end